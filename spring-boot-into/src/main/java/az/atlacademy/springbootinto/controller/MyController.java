package az.atlacademy.springbootinto.controller;

import az.atlacademy.springbootinto.dto.Account;
import az.atlacademy.springbootinto.service.MyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api")
public class MyController {
   // @Autowired
    private  MyService ms;

    public MyController(MyService ms) {
        this.ms = ms;
    }

   /* void inject(MyService ms){
        this.ms = ms;
    }*/
  //  @GetMapping("/hello")//  @PutMapping  @PostMapping @DeleteMapping
    @RequestMapping(value = "/hello",method = RequestMethod.GET)
    @ResponseBody
    public String getHello(){
        return "Hello World!!! "+ LocalDateTime.now();
    }

    @GetMapping("/now")
    @ResponseBody
    public String getNow(){
        return LocalDateTime.now().toString();
    }

    @GetMapping("/names")
    @ResponseBody
    public List<String> getNames(){
        return ms.getNames();
    }

    @PostMapping("/save")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<String> save(@RequestBody Account account){
        System.out.println("Account "+account);
        if (account.getName().length()<6){
            return  new ResponseEntity<>(
                    String.format("%s %s",account.getCustomer(),account.getName()),
                    HttpStatus.BAD_REQUEST);
        }
        return  new ResponseEntity<>(
                String.format("%s %s",account.getCustomer(),account.getName()),
                HttpStatus.CREATED);
             //   ResponseEntity.ok(String.format("%s %s",account.getCustomer(),account.getName()));
    }
}
