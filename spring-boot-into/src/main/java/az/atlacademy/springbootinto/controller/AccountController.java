package az.atlacademy.springbootinto.controller;


import az.atlacademy.springbootinto.dto.Account;
import az.atlacademy.springbootinto.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/accounts")
@RequiredArgsConstructor

public class AccountController {

    private final  AccountService accountService;



    @GetMapping
    public ResponseEntity<List<Account>> accounts(){
        return ResponseEntity.ok(accountService.getList());
    }

    @GetMapping(value = "/{accountId}")
    public ResponseEntity<Account> getAccountById(@PathVariable("accountId") Long id){
        return  ResponseEntity.ok( accountService.getAccountById(id));
    }

    @GetMapping(value = "/query")
    public ResponseEntity<Account> getAccountByIdQuery(@RequestParam("accountId") Long id){
        return  ResponseEntity.ok( accountService.getAccountById(id));
    }
    @PostMapping
    public ResponseEntity<Account> save(@RequestBody Account account){
        return  ResponseEntity.ok( accountService.create(account));
    }

    @PutMapping
    public ResponseEntity<Account> update(@RequestBody Account account){
        return  ResponseEntity.ok( accountService.update(account));
    }

    @DeleteMapping
    public ResponseEntity<Account> delete(@RequestBody Account account){
        return  ResponseEntity.ok( accountService.delete(account));
    }
}
