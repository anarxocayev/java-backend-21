package az.atlacademy.springbootinto.service;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TestController {
    private  MyService ms;

    public TestController(MyService ms) {
        this.ms = ms;
    }
    @GetMapping("/names2")
    public List<String> getNames(){
        return ms.getNames();
    }
}
