package az.atlacademy.springbootinto.service;

import az.atlacademy.springbootinto.dto.Account;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AccountService {

    @Value("${tax.percent}")
    private String taxPercent;

    private static List<Account> accounts = new ArrayList<>();

    static {


        accounts.add(new Account(1L, "ACC001AZN", "Anar Xocayev"));
        accounts.add(new Account(2L, "ACC004AZN", "Nihad Aliyev"));
        accounts.add(new Account(3L, "ACC002AZN", "Kamil Aliyev"));
        accounts.add(new Account(4L, "ACC003AZN", "Vuqar Sadiqov"));
        accounts.add(new Account(5L, "ACC006AZN", "Araz Ismayilov"));
        accounts.add(Account.builder().id(23l).name("ACC234AZN").customer("Kamal Veliyev").build());

    }
    public List<Account> getList() {
        System.out.println("TAX Percent "+taxPercent);
        return accounts;
    }
    public Account getAccountById(Long id) {
        return accounts.stream().filter(a -> a.getId().equals(id)).findFirst().get();
    }
    public Account create(Account account) {
        account.setId((long) (Math.random() * 100));
        accounts.add(account);
        return account;
    }

    public Account update(Account account) {
        Account forUppdate = getAccountById(account.getId());
        forUppdate.setId(account.getId());
        forUppdate.setCustomer(account.getCustomer());
        forUppdate.setName(account.getName());
        return forUppdate;

    }

    public Account delete(Account account) {
        Account forDelete=getAccountById(account.getId());
        accounts.remove(forDelete);
        return account;
    }
}
