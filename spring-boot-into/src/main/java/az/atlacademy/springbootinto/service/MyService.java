package az.atlacademy.springbootinto.service;

import az.atlacademy.springbootinto.config.MyTestClass;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MyService {
    MyTestClass myTestClass;

    public MyService(MyTestClass myTestClass) {
        this.myTestClass = myTestClass;
    }

    public List<String> getNames(){
        List<String> name=new ArrayList<>();
        name.add("Anar");
        name.add("Ayhan");
        name.add("Meqsed");

      myTestClass.names().forEach(n->name.add(n));

       return name;
    }
}
