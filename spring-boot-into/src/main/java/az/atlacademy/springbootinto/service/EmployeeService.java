package az.atlacademy.springbootinto.service;

import az.atlacademy.springbootinto.entity.Employee;
import az.atlacademy.springbootinto.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    private EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> getEmployees() {
        return employeeRepository.findAll();
    }

    public String create(Employee employee) {
       int res =   employeeRepository.save(employee);
          return res>0?"SUCCESS":"ERROR";
    }

    public Employee getEmployeeById(Integer id) {
        return employeeRepository.findById(id);
    }

    public String delete(Integer id) {
        Employee employee=employeeRepository.findById(id);
        int res =   employeeRepository.delete(employee);
        return res>0?"SUCCESS":"ERROR";
    }
}
