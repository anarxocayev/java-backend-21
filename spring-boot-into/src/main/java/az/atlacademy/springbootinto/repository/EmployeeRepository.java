package az.atlacademy.springbootinto.repository;


import az.atlacademy.springbootinto.config.DBConnection;
import az.atlacademy.springbootinto.entity.Department;
import az.atlacademy.springbootinto.entity.Employee;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
@Repository
public class EmployeeRepository {
    private DBConnection dbConnection = new DBConnection();


    public int save(Employee emp) {
        int qresult = 0;
        try (Connection conn = dbConnection.getConnection()) {

            String sqlInsert = "insert into employees " +
                    "            values(nextval('employee_id'), ?, ?, ?, " +
                    "                    ?,? , ?)";
            // Employee emp = getNewEmployee();
            PreparedStatement psIns = conn.prepareStatement(sqlInsert);


            psIns.setString(1, emp.getName());
            psIns.setString(2, emp.getSurname());
            psIns.setString(3, emp.getEmail());
            psIns.setString(4, emp.getPhone());
            psIns.setString(5, emp.getAddress());
            psIns.setInt(6, emp.getDepartment().getId());
            qresult = psIns.executeUpdate();
            System.out.println(qresult);
            if (qresult != 0) {
                System.out.println("Inserted..");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return qresult;
    }

    public int update(Employee emp) {
        int qresult = 0;
        try (Connection conn = dbConnection.getConnection()) {

            String sqlInsert = "update employees " +
                    " set name=?,surname=?,email=?,phone=?,address=?,dept_id=? where id=?";
            // Employee emp = getNewEmployee();
            PreparedStatement psIns = conn.prepareStatement(sqlInsert);

            psIns.setString(1, emp.getName());
            psIns.setString(2, emp.getSurname());
            psIns.setString(3, emp.getEmail());
            psIns.setString(4, emp.getPhone());
            psIns.setString(5, emp.getAddress());
             psIns.setInt(6, emp.getDepartment().getId());

            psIns.setInt(7, emp.getId());
            qresult = psIns.executeUpdate();
            System.out.println(qresult);
            if (qresult != 0) {
                System.out.println("Updated..");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return qresult;
    }

    public int delete(Employee emp) {
        int qresult = 0;
        try (Connection conn = dbConnection.getConnection()) {

            String sqlDelete = "delete from  employees  where id=?";
            PreparedStatement psIns = conn.prepareStatement(sqlDelete);
            psIns.setInt(1, emp.getId());

            qresult = psIns.executeUpdate();
            System.out.println(qresult);
            if (qresult != 0) {
                System.out.println("Deleted..");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return qresult;
    }

    public List<Employee> findAll() {

        List<Employee> list = new ArrayList<>();
        try (Connection conn = dbConnection.getConnection()) {
            String sqlQuery = "select  * from employees order by id ";
            PreparedStatement ps = conn.prepareStatement(sqlQuery);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                Employee e = new Employee(resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("surname"),
                        resultSet.getString("email"),
                        resultSet.getString("phone"),
                        resultSet.getString("address"),
                        findDepartmentById(resultSet.getInt("dept_id"))
                        // resultSet.getInt("dept_id")
                );

                list.add(e);
                // System.out.println(e.toString());
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return list;
    }

    public List<Employee> findByDepartment(Department department) {

        List<Employee> list = new ArrayList<>();
        try (Connection conn = dbConnection.getConnection()) {
            String sqlQuery = "select  * from employees where dept_id=? order by id ";
            PreparedStatement ps = conn.prepareStatement(sqlQuery);
            ps.setInt(1, department.getId());
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                Employee e = new Employee(resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("surname"),
                        resultSet.getString("email"),
                        resultSet.getString("phone"),
                        resultSet.getString("address"),
                        findDepartmentById(resultSet.getInt("dept_id"))
                        // resultSet.getInt("dept_id")
                );

                list.add(e);
                // System.out.println(e.toString());
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return list;
    }


    public Employee findById(int id) {

        try (Connection conn = dbConnection.getConnection()) {
            String sqlQuery = "select  * from employees where id=?";
            PreparedStatement ps = conn.prepareStatement(sqlQuery);
            ps.setInt(1, id);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                return new Employee(resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("surname"),
                        resultSet.getString("email"),
                        resultSet.getString("phone"),
                        resultSet.getString("address"),
                        findDepartmentById(resultSet.getInt("dept_id"))

                        //  resultSet.getInt("dept_id")
                );


            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;

    }

    public Department findDepartmentById(int id) {

        try (Connection conn = dbConnection.getConnection()) {
            String sqlQuery = "select  * from departments where id=?";
            PreparedStatement ps = conn.prepareStatement(sqlQuery);
            ps.setInt(1, id);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                Department dept = new Department(resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("phone"),
                        new ArrayList<>()
                );



                return dept;

            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;

    }
}
