package az.atlacademy.springbootinto.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


//create sequence department_id start with 100;

  //  select nextval('employee_id');

public class DBConnection {
    private  Connection conn ;
    public Connection getConnection(){


        String url = "jdbc:postgresql://localhost:5432/java-backend";
        String user = "postgres";
        String pass = "root";
        System.out.println("Connecting.....");

        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(url, user, pass);
        } catch (SQLException | ClassNotFoundException throwables) {
            System.out.println(throwables.getMessage());
        }
        return conn;

    }


}
