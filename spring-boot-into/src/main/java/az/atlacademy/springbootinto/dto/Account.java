package az.atlacademy.springbootinto.dto;

import lombok.*;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder

public class Account {

    private Long id;

    private String name;
    private String customer;


}

