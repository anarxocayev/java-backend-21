package az.atlacademy.springbootinto.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Student {
    private Long id;
    private String name;
    private String surname;
    private String email;
    private String address;
    private BigDecimal fee;

}
