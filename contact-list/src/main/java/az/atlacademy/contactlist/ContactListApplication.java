package az.atlacademy.contactlist;

import az.atlacademy.contactlist.client.feinclient.ClientEgov;
import az.atlacademy.contactlist.client.feinclient.ClientHospital;
import az.atlacademy.contactlist.client.feinclient.ClientUser;
import az.atlacademy.contactlist.client.resttemplateex.ClientUserResttemplate;
import az.atlacademy.contactlist.client.resttemplateex.HospitalAuth;
import az.atlacademy.contactlist.controller.LoginRequest;
import az.atlacademy.contactlist.entity.manytomany.Product;
import az.atlacademy.contactlist.entity.manytomany.Store;
import az.atlacademy.contactlist.repository.ContactRepository;
import az.atlacademy.contactlist.repository.PersonRepository;
import az.atlacademy.contactlist.repository.ProductRepository;
import az.atlacademy.contactlist.repository.StoreRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.transaction.Transactional;

@SpringBootApplication
@RequiredArgsConstructor
@Transactional
@Slf4j
public class ContactListApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(ContactListApplication.class, args);
    }

    private final ContactRepository contactRepository;
    private final PersonRepository personRepository;
    private final ProductRepository productRepository;
    private final StoreRepository storeRepository;

    private final ClientHospital  clientHospital;
    private final ClientEgov clientEgov;
    private final ClientUser clientUser;
    private final ClientUserResttemplate clientUserResttemplate;
    private final HospitalAuth hospitalAuth;


    @Override
    public void run(String... args) throws Exception {

       /* Person p = new Person();
        p.setName("Anar");
        p.setSurname("Xocayev");
        p = personRepository.save(p);

        Contact c1=new Contact();
        c1.setValue("0556122116");
        c1.setContactType(ContactType.PHONE);
        c1.setPerson(p);
        contactRepository.save(c1);

        Contact c2=new Contact();
        c2.setValue("0512297762");
        c2.setContactType(ContactType.PHONE);
        c2.setPerson(p);
        contactRepository.save(c2);

        Contact c3=new Contact();
        c3.setValue("anarxocayev@gmail.com");
        c3.setContactType(ContactType.EMAIL);
        c3.setPerson(p);
        contactRepository.save(c3);

        Contact c4=new Contact();
        c4.setValue("anar.xocayev");
        c4.setContactType(ContactType.SKYPE);
        c4.setPerson(p);
        contactRepository.save(c4);*/

    //    Person p=personRepository.findById(1L).get();
      //  System.out.println(p);
        Store s=storeRepository.findById(2L).get();
      //  System.out.println(s);

        Product p=productRepository.findById(22L).get();
        //System.out.println(p);
       // System.out.println(p.getStores());

     //   System.out.println("ALL HOSPITALS : "+clientHospital.getHospitals());
       log.info("ALL DISTRICTS : {}",clientEgov.getDistricts());
        //log.info("ALL users : {}",clientUser.getUsers());
      //  log.info("ALL users : {}",clientUserResttemplate.getAllUsers());
        //log.info("token {}", hospitalAuth.login(new LoginRequest("axocayev","test1234")));


    }
}
