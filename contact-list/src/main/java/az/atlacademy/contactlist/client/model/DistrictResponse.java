package az.atlacademy.contactlist.client.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor

public class DistrictResponse {
    private String Status;
    private String StatusMessage;
    private List<DistrictShort> Response;
}
