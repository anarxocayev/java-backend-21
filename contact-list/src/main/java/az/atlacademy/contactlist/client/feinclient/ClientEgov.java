package az.atlacademy.contactlist.client.feinclient;

import az.atlacademy.contactlist.client.model.DistrictResponse;
import az.atlacademy.contactlist.client.model.HospitalResponse;
import az.atlacademy.contactlist.config.FeignConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "egovdata", url = "https://api.opendata.az/v1/json/map",configuration = FeignConfig.class)

public interface ClientEgov {
    @RequestMapping(method = RequestMethod.GET, value = "/districts")
    DistrictResponse getDistricts();
}
