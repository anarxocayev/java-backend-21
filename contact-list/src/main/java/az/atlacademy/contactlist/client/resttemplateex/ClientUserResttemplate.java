package az.atlacademy.contactlist.client.resttemplateex;


import az.atlacademy.contactlist.client.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.client.loadbalancer.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
@RequiredArgsConstructor

public class ClientUserResttemplate {
    private final RestTemplate restTemplate;

    public List<User> getAllUsers() {
        ResponseEntity<List> responseEntity = restTemplate.getForEntity("https://60abbbdf5a4de40017ccac2a.mockapi.io/api/v1/users", List.class);
        return (List<User> ) responseEntity.getBody();
    }

}
