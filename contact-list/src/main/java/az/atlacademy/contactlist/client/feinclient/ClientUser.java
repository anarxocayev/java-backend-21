package az.atlacademy.contactlist.client.feinclient;

import az.atlacademy.contactlist.client.model.HospitalResponse;
import az.atlacademy.contactlist.client.model.User;
import org.springframework.boot.web.embedded.undertow.UndertowServletWebServer;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(value = "users", url = "https://60abbbdf5a4de40017ccac2a.mockapi.io/api/v1")
public interface ClientUser {

    @RequestMapping(method = RequestMethod.GET, value = "/users")
    List<User> getUsers();

}
