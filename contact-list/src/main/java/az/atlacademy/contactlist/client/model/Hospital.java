package az.atlacademy.contactlist.client.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Hospital {
    private Long id;
    private String name;
    private String address;
    private String email;
    private String phone;
}
