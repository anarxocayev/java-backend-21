package az.atlacademy.contactlist.client.model;

import lombok.Data;

import java.util.List;

@Data
public class HospitalResponse {
    private  List<Hospital> data;
}
