package az.atlacademy.contactlist.client.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DistrictShort {
    Long objectId;
    String districtId;
    String name;
    String nameEn;

}
