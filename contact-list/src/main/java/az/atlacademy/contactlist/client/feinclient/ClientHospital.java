package az.atlacademy.contactlist.client.feinclient;


import az.atlacademy.contactlist.client.model.HospitalResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@FeignClient(value = "jplaceholder", url = "localhost:8080/api")
public interface ClientHospital {


    @RequestMapping(method = RequestMethod.GET, value = "/hospitals")
    HospitalResponse getHospitals();

}
