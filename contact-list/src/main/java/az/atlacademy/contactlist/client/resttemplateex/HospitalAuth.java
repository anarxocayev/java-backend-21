package az.atlacademy.contactlist.client.resttemplateex;

import az.atlacademy.contactlist.controller.JwtAuthenticationResponse;
import az.atlacademy.contactlist.controller.LoginRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
public class HospitalAuth {

    private final RestTemplate restTemplate;
    public JwtAuthenticationResponse login(LoginRequest loginRequest){
       // HttpEntity<LoginRequest> request = new HttpEntity<>(new LoginRequest("axocayev","test1234"));
        HttpEntity<LoginRequest> request = new HttpEntity<>(loginRequest);

        JwtAuthenticationResponse response = restTemplate.postForObject("http://localhost:8080/api/auth/signin", request, JwtAuthenticationResponse.class);
        return  response;


    }

}
