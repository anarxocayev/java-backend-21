package az.atlacademy.contactlist.entity.manytomanythirdtable;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    private String name;

    @ManyToMany
    Set<Student> likes;


}