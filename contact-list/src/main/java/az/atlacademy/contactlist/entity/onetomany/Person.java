package az.atlacademy.contactlist.entity.onetomany;


import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Data
@Entity
@Table(name = "persons")
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String surname;

    @OneToMany
    @JoinColumn(name = "person_id")
    private List<Contact> contactList=new ArrayList<>();

}
 //1:PERSON-> N:CONTACT