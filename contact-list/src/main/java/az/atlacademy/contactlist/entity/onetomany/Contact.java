package az.atlacademy.contactlist.entity.onetomany;

import az.atlacademy.contactlist.enums.ContactType;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

@Data
@Entity
@Table(name = "contacts")
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    private ContactType contactType;
    private String value;

    @ManyToOne
    @ToString.Exclude
    private Person person;

}
