package az.atlacademy.contactlist.entity;


public enum  RoleName {

    ROLE_USER,
    ROLE_ADMIN,
    ROLE_OPERATOR
}
