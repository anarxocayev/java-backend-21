package az.atlacademy.contactlist.entity.manytomany;

import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "stores")
public class Store {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;

    @ManyToMany
    @JoinTable(name = "store_product",
            joinColumns = { @JoinColumn(name ="store_id") },
            inverseJoinColumns = { @JoinColumn(name = "product_id") })
   // @ToString.Exclude
    private Set<Product> products = new HashSet<>();
}
