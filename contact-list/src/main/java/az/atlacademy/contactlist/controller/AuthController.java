package az.atlacademy.contactlist.controller;

import az.atlacademy.contactlist.repository.UserRepository;
import az.atlacademy.contactlist.security.JwtTokenProvider;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collections;
import java.util.Set;


@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
@Slf4j

public class AuthController {


    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider tokenProvider;



    @PostMapping("/signin")

    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        log.info(loginRequest.getUsernameOrEmail() + " " + loginRequest.getPassword() + " user ");
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsernameOrEmail(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);
        log.info("jwt token {}", jwt);
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
    }


/*
    @PostMapping("/signup")
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "", authorizations = {@Authorization(value = "jwtToken")})
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
        System.out.println(signUpRequest);
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return new ResponseEntity(new ApiResponse(false, "Username is already taken!"),
                    HttpStatus.BAD_REQUEST);
        }



        // Creating user's account
        User user = new User(signUpRequest.getName(), signUpRequest.getUsername(),
                signUpRequest.getEmail(), signUpRequest.getPassword());
        user.setIsActive(1);

        user.setPassword(passwordEncoder.encode(user.getPassword()));


        Role userRole = roleRepository.findById(signUpRequest.getRoleId())
                .orElseThrow(() -> new AppException("User Role not set."));

        user.setRoles(Collections.singleton(userRole));

        User result = userRepository.save(user);



        return ResponseEntity.ok((new ApiResponse(true, "User registered successfully")));
    }

    @PostMapping("/user/edit")
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "", authorizations = {@Authorization(value = "jwtToken")})
    public ResponseEntity<?> editUser(@Valid @RequestBody SignUpRequest signUpRequest) {
//        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
//            return new ResponseEntity(new ApiResponse(false, "Username is already taken!"),
//                    HttpStatus.BAD_REQUEST);
//        }
        log.info("editUser signUpRequest  {}", signUpRequest);
        User user = userRepository.getOne(signUpRequest.getId());

        if (!userRepository.findByEmail(signUpRequest.getEmail()).get().getEmail().equals(user.getEmail())) {
            return new ResponseEntity(new ApiResponse(false, "Email Address already in use!"),
                    HttpStatus.BAD_REQUEST);
        }


        if (signUpRequest.getEmail() != null) {
            user.setEmail(signUpRequest.getEmail());
        }
        if (signUpRequest.getName() != null) {
            user.setName(signUpRequest.getName());
        }


        if (signUpRequest.getPassword() != null) {
            user.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));
        }



        if (signUpRequest.getRoleId() != null && signUpRequest.getRoleId() != 0) {
            Set<Role> roles = user.getRoles();
            roles.clear();
            user.setRoles(roles);
            userRepository.save(user);
            Role userRole = roleRepository.findById(signUpRequest.getRoleId())
                    .orElseThrow(() -> new AppException("User Role not set."));
            roles.add(userRole);
            user.setRoles(roles);

        }
        log.info("editUser user  {}", user);
        User result = userRepository.save(user);
        log.info("editUser result  {}", result);
       /* URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/users/{username}")
                .buildAndExpand(result.getUsername()).toUri()

        return ResponseEntity.ok((new ApiResponse(true, "User registered successfully")));
    }*/
}
