package az.atlacademy.contactlist.repository;

import az.atlacademy.contactlist.entity.manytomany.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product,Long> {
}
