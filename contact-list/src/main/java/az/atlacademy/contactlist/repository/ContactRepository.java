package az.atlacademy.contactlist.repository;

import az.atlacademy.contactlist.entity.onetomany.Contact;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactRepository extends JpaRepository<Contact,Long> {
}
