package az.atlacademy.contactlist.repository;

import az.atlacademy.contactlist.entity.manytomanythirdtable.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student ,Long> {
}
