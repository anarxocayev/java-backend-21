package az.atlacademy.contactlist.repository;

import az.atlacademy.contactlist.entity.manytomany.Store;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreRepository extends JpaRepository<Store,Long> {
}
