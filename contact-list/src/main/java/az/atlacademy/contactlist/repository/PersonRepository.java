package az.atlacademy.contactlist.repository;

import az.atlacademy.contactlist.entity.onetomany.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person,Long> {
}
