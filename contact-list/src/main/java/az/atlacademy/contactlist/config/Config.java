package az.atlacademy.contactlist.config;

import az.atlacademy.contactlist.client.feinclient.ClientEgov;
import az.atlacademy.contactlist.client.feinclient.ClientHospital;
import az.atlacademy.contactlist.client.feinclient.ClientUser;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableFeignClients(clients = {ClientHospital.class, ClientEgov.class, ClientUser.class})
public class Config {

    @Bean
    RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
