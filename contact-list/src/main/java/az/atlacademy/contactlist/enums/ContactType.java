package az.atlacademy.contactlist.enums;

public enum ContactType {
    EMAIL,PHONE,ADDRESS,SKYPE,LINKEDIN,FACEBOOK
}
