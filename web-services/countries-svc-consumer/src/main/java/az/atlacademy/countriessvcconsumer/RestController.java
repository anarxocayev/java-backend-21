package az.atlacademy.countriessvcconsumer;


import com.example.consumingwebservice.wsdl.GetCountryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.awt.*;

@org.springframework.web.bind.annotation.RestController
public class RestController {
    @Autowired
   private CountryClient countryClient;

    @GetMapping(value = "/country/{name}" ,produces  = MediaType.APPLICATION_JSON_VALUE)
    public GetCountryResponse getCountryResponse(@PathVariable String name){
        return countryClient.getCountry(name);
    }

}
