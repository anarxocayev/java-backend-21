package az.atlacademy.web.svc.producingwebservice;

import io.spring.guides.gs_producing_web_service.Country;
import io.spring.guides.gs_producing_web_service.Currency;
import io.spring.guides.gs_producing_web_service.Region;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
@Component
public class CountryRepository {

    private static final Map<String, Country> countries = new HashMap<>();

    @PostConstruct
    public void initData() {
        Region regionAmerica=new Region();
        regionAmerica.setId(1);
        regionAmerica.setName("America");

        Region regionEurope=new Region();
        regionEurope.setId(1);
        regionEurope.setName("Europe");


        Country spain = new Country();
        spain.setName("Spain");
        spain.setCapital("Madrid");
        spain.setCurrency(Currency.EUR);
        spain.setPopulation(46704314);
        spain.setRegion(regionEurope);

        countries.put(spain.getName(), spain);

        Country poland = new Country();
        poland.setName("Poland");
        poland.setCapital("Warsaw");
        poland.setCurrency(Currency.PLN);
        poland.setPopulation(38186860);
        poland.setRegion(regionEurope);

        countries.put(poland.getName(), poland);

        Country uk = new Country();
        uk.setName("United Kingdom");
        uk.setCapital("London");
        uk.setCurrency(Currency.GBP);
        uk.setPopulation(63705000);
        uk.setRegion(regionEurope);

        countries.put(uk.getName(), uk);

        Country usa = new Country();
        usa.setName("United States of America");
        usa.setCapital("London");
        usa.setCurrency(Currency.GBP);
        usa.setPopulation(433705000);
        usa.setRegion(regionAmerica);

        countries.put(usa.getName(), usa);
    }

    public Country findCountry(String name) {
        Assert.notNull(name, "The country's name must not be null");
        return countries.get(name);
    }
}
