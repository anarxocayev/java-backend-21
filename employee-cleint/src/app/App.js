import React, {Component} from 'react';
import './App.css';
import {Redirect, Route, Switch, withRouter} from 'react-router-dom';

import {getCurrentUser} from '../util/APIUtils';
import {ACCESS_TOKEN} from '../constants';


import AppHeader from '../common/AppHeader';
import NotFound from '../common/NotFound';
import LoadingIndicator from '../common/LoadingIndicator';
import MainMenu from '../common/Menu';


import {Layout, notification} from 'antd/lib';
import 'antd/dist/antd.css';
import Hospitals from "../pages/Hospitals";

const {Content, Footer, Sider} = Layout;

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // currentUser: null,
            // isAuthenticated: false,
            // isLoading: false,
             collapsed: false,

        }
        // this.handleLogout = this.handleLogout.bind(this);
        // this.loadCurrentUser = this.loadCurrentUser.bind(this);
        // this.handleLogin = this.handleLogin.bind(this);

        notification.config({
            placement: 'topRight',
            top: 70,
            duration: 25,
        });
    }


    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    loadCurrentUser() {
        this.setState({
            isLoading: true
        });
        // getCurrentUser()
        //     .then(response => {
        //         this.setState({
        //             currentUser: response,
        //             isAuthenticated: true,
        //             isLoading: false
        //         });
        //     }).catch(error => {
        //     this.setState({
        //         isLoading: false
        //     });
        // });
    }


    componentDidMount() {
        //this.loadCurrentUser();
    }

    handleLogout(redirectTo = "/login", notificationType = "success", description = "You're successfully logged out.") {
        localStorage.removeItem(ACCESS_TOKEN);

        this.setState({
            currentUser: null,
            isAuthenticated: false
        });

        this.props.history.push(redirectTo);

        notification[notificationType]({
            message: 'StreamSoft Tutorials',
            description: description,
        });
    }

    handleLogin() {
        notification.success({
            message: 'StreamSoft Tutorials',
            description: "You're successfully logged in.",
        });

        this.props.history.push("/");
    }

    render() {

        const menu = (

            <MainMenu/>

        )

        // if (this.state.isLoading) {
        //     return <LoadingIndicator/>
        // }
        return (
            <Layout className="app-container">
                <AppHeader isAuthenticated={this.state.isAuthenticated}
                           currentUser={this.state.currentUser}
                           onLogout={this.handleLogout}

                />


                <Layout className="app-content">
                    {/*<Sider trigger={null} collapsible collapsed={this.state.collapsed}*/}
                    {/*       style={{background: '#f0f2f5'}}>*/}

                    {/*    { menu }*/}

                    {/*</Sider>*/}
                    <Content>
                        <Switch>
                            <Route exact path="/"
                                   render={(props) =>
                                       <Redirect to='/dashboard'/>
                                   }>
                            </Route>

                            <Route path="/hospitals"
                                   render={(props) =>
                                       <Hospitals  {...props}  />}>

                            </Route>


                            <Route component={NotFound}></Route>
                        </Switch>

                    </Content>

                </Layout>

                <Footer
                    style={{textAlign: 'center', bottom: "0", position: "relative", clear: "both", height: "600px"}}>


                    mia.gov.az ©2020

                </Footer>
            </Layout>
        );
    }
}

export default withRouter(App);
