import React, {Component} from 'react';


import {Table} from 'antd';
import {getHospitalComments, getHospitals} from "../util/APIUtils";


class Hospitals extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            hospitalId: -1,
            hospitalInfo: {},
            commentList: []
        }


        this.loadHospitlas = this.loadHospitlas.bind(this);
    }

    loadHospitlas = () => {

        console.log("start loading ")

        getHospitals()
            .then(response => {
                this.setState({
                    data: response.data
                });
                console.log('data ', this.data[0].name);
            }).catch(error => {
            if (error.status === 404) {

                console.log('404 ');
            } else {

                console.log('error ', error);
            }
        });
    }


    loadComments = () => {
        console.log("inside of methoid " + this.state.hospitalId)
        console.log("start loading ")
        getHospitalComments(this.state.hospitalId)
            .then(response => {
                this.setState({
                    hospitalInfo: response.data,
                    commentList: response.data.commentList

                });
                console.log('comments  ', this.state.hospitalInfo);
            }).catch(error => {
            if (error.status === 404) {
                console.log('404 ');
            } else {
                console.log('error ', error);
            }
        });
    }


    componentDidMount() {

        this.loadHospitlas();

    }


    render() {


        const columns = [
            {title: 'Kod', dataIndex: 'id', key: 'id'},
            {title: 'Adi`', dataIndex: 'name', key: 'name'},
            {title: 'Addressi', dataIndex: 'address', key: 'address'},
            {title: 'Telefon', dataIndex: 'phone', key: 'phone'},
            {title: 'Email', dataIndex: 'email', key: 'email'},
            {title: 'Komment Sayi', dataIndex: 'commentCount', key: 'commentCount'},

        ];


        return (
            <div className="container">


                <Table dataSource={this.state.data}
                       columns={columns}
                       onRow={(record, rowIndex) => {
                           return {
                               onClick: event => {
                                   this.setState({
                                      // hospitalId: record.id,
                                       image:record.image
                                   })
                                   console.log(this.state.hospitalId);
                                   console.log('rowIndex  ' + rowIndex);
                                   this.loadComments()
                               }, // click row
                               onDoubleClick: event => {
                               }, // double click row
                               onContextMenu: event => {
                               }, // right button click row
                               onMouseEnter: event => {
                                   this.setState({
                                       hospitalId: record.id,

                                   })
                                   // console.log(this.state.hospitalId);
                                   // console.log('rowIndex  ' + rowIndex);
                                   // this.loadComments(this.state.hospitalId)
                               }, // mouse enter row
                               onMouseLeave: event => {
                               }, // mouse leave row
                           };
                       }}/>


                <div>
                    <img src={'http://localhost:8080/file/images/'+this.state.image}/>
                </div>

                <div>

                    {
                        this.state.commentList.map((comment, key) =>


                            <div>
                                <h4> {comment.author} {comment.email}</h4>
                                <p>{comment.comment} </p>

                            </div>
                        )

                    }
                </div>

            </div>

        );

    }
}

export default Hospitals;
