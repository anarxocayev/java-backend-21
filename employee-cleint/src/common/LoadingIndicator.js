import React from 'react';
import { LoadingOutlined } from '@ant-design/icons';

export default function LoadingIndicator(props) {
    
    return (
        <LoadingOutlined />
    );
}
