import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import './AppHeader.css';
import {DatePicker, Dropdown, Form, Input, Layout, Menu, Modal, Tooltip} from 'antd';
import {DownOutlined, LoginOutlined, UserOutlined} from '@ant-design/icons';

const Header = Layout.Header;
const {TextArea} = Input;


const CreateToDoList = ({visible,onCreate,onCancel}) => {
    const [form] = Form.useForm();
    form.setFieldsValue({

    });

    return (


        <Modal
            visible={visible}
            title="Yeni qeyd "
            okText="Əlavə et"
            cancelText="Ləğv et"
            onCancel={onCancel}
            onOk={() => {
                form.validateFields()
                    .then(values => {
                        form.resetFields();
                        onCreate(values);
                    })
                    .catch(info => {
                        console.log('Validate Failed:', info);
                    });
            }}
        >
            <Form form={form} labelCol={{span: 6}}
                  wrapperCol={{span: 24}}
                  layout="horizontal">

                <Form.Item name="todo" label="Qeyd" rules={[{required: true}]}>
                    <TextArea rows={6}/>
                </Form.Item>

                <Form.Item label="Başalam tarixi" name="dateFrom"
                           rules={[{required: true, message: 'Zəhmət olmas başlama tarixini daxil edin!'}]}>
                    <DatePicker placeholder="DD-MM-YYYY'" format={'DD-MM-YYYY'}/>
                </Form.Item>
                <Form.Item label="Bitmə tarixi" name="dateTo"
                           rules={[{required: true, message: 'Zəhmət olmas bitmə tarixini daxil edin!'}]}>
                    <DatePicker placeholder="DD-MM-YYYY'" format={'DD-MM-YYYY'}/>
                </Form.Item>

            </Form>
        </Modal>
    );
};


class AppHeader extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            createToDoListVisible:false
        })
        this.handleMenuClick = this.handleMenuClick.bind(this);
    }

    handleMenuClick({key}) {
        if (key === "logout") {
            this.props.onLogout();
        }
    }

    showModal=()=>{
        this.setState({
            createToDoListVisible:true
        })
    }
    ModalCancel = () => {
        this.setState({
            createToDoListVisible: false,
            }
        )
    }


    render() {
        let menuItems;
        if (this.props.currentUser) {
            menuItems = [

                <Menu.Item key="/profile" className="profile-menu">

                    <ProfileDropdownMenu
                        currentUser={this.props.currentUser}
                        handleMenuClick={this.handleMenuClick}/>

                </Menu.Item>,

            ];
        } else {
            menuItems = [

                <Menu.Item key="/login">
                    <Tooltip title="Giriş">
                        <Link to="/login">
                            <LoginOutlined/> Giriş
                        </Link>
                    </Tooltip>
                </Menu.Item>

            ];
        }

        return (
            <Header className="app-header" style={{background: "#fff"}}>
                <div className="container">
                    {/*<div className="app-title">*/}

                    {/*    <Link to="/" style={{color: "#000"}}>*/}
                    {/*        <img src="/logo.png" width="35px" height="50px"/>Hotels</Link>*/}
                    {/*</div>*/}
                    <Menu
                        className="app-menu"
                        mode="horizontal"
                        selectedKeys={[this.props.location.pathname]}
                        style={{lineHeight: '64px'}}>
                        {menuItems}
                    </Menu>
                </div>

                <CreateToDoList
                    visible={this.state.createToDoListVisible}
                    onCreate={this.handleCreateTodoList}
                    onCancel={this.ModalCancel}
                />
            </Header>
        );
    }
}

function ProfileDropdownMenu(props) {
    const dropdownMenu = (
        <Menu onClick={props.handleMenuClick} className="profile-dropdown-menu">
            <Menu.Item key="user-info" className="dropdown-item" disabled>
                <div className="user-full-name-info">
                    {props.currentUser.name}
                </div>
                <div className="username-info">
                    @{props.currentUser.username}
                </div>
            </Menu.Item>
            <Menu.Divider/>
            <Menu.Item key="profile" className="dropdown-item">
                <Link to={`/profile`}>Profile</Link>
            </Menu.Item>
            <Menu.Item key="logout" className="dropdown-item">
                Logout
            </Menu.Item>
        </Menu>
    );

    return (
        <Dropdown
            overlay={dropdownMenu}
            trigger={['click']}
            getPopupContainer={() => document.getElementsByClassName('profile-menu')[0]}>
            <a className="ant-dropdown-link">
                <UserOutlined />{props.currentUser.name} <DownOutlined/>
            </a>
        </Dropdown>
    );
}





export default withRouter(AppHeader);
