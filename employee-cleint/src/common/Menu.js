import React, {Component} from 'react';

import {Layout, Menu} from 'antd';
import {Link} from 'react-router-dom';
import {DashboardOutlined} from '@ant-design/icons';
import './Menu.css'

const {Header, Content, Footer, Sider} = Layout;
const {SubMenu} = Menu;

class MainMenu extends Component {
    state = {
        collapsed: false,
    };


    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    render() {
        return (


            <Menu theme="light" defaultSelectedKeys={['0']} mode="inline" style={{width: 220}}>
                <Menu.Item key="dashboard" icon={<DashboardOutlined/>}>
                    <Link to={'/dashboard'} style={{color: "rgba(0, 0, 0, 0.65)"}}>Statistika</Link>
                </Menu.Item>

            </Menu>

        );
    }
}

export default MainMenu;
