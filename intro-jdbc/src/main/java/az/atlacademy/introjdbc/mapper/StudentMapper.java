package az.atlacademy.introjdbc.mapper;

import az.atlacademy.introjdbc.model.Student;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class StudentMapper implements RowMapper<Student> {

    @Override
    public Student mapRow(ResultSet rs, int rowNum) throws SQLException {
        Student s = new Student();
        s.setId(rs.getLong("id"));
        s.setName(rs.getString("name"));
        s.setSurname(rs.getString("surname"));
        s.setEmail(rs.getString("email"));
        s.setAddress(rs.getString("address"));
        s.setUsername(rs.getString("username"));
        s.setPassword(rs.getString("password"));
        return s;
    }
}
