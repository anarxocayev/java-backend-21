package az.atlacademy.introjdbc.repository;

public class SQLConstants {
    public static final String SQL_FIND_STUDENT = "select * from student where id = ?";
    public static final String SQL_DELETE_STUDENT = "delete from student where id = ?";
    public static final String SQL_UPDATE_STUDENT = "update student set name = ?, surname = ?," +
            " address  = ? where id = ?";
    public static final String SQL_GET_ALL = "select * from student";
    public static final String SQL_INSERT_STUDENT = "insert into student(id, name, surname, address) " +
            "values(?,?,?,?)";
}
