package az.atlacademy.introjdbc.repository;

import az.atlacademy.introjdbc.mapper.StudentMapper;
import az.atlacademy.introjdbc.model.Student;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class StudentRepository implements StudentDao {
    private final JdbcTemplate jdbcTemplate;


    @Override
    public Student getStudentById(Long id) {
        return jdbcTemplate.queryForObject(SQLConstants.SQL_FIND_STUDENT,new Object[]{id},new StudentMapper());
    }

    @Override
    public List<Student> getAllStudents() {
        return jdbcTemplate.query(SQLConstants.SQL_GET_ALL,new StudentMapper());
    }

    @Override
    public boolean deletePerson(Student student) {
        return jdbcTemplate.update(SQLConstants.SQL_DELETE_STUDENT, new Object[]{student.getId()} )>0?true:false;
    }

    @Override
    public boolean updatePerson(Student student) {
        return jdbcTemplate.update(SQLConstants.SQL_UPDATE_STUDENT, new Object[]{
                student.getName(),
                student.getSurname(),
                student.getAddress(),
                student.getId()} )>0?true:false;
    }

    @Override
    public boolean createPerson(Student student) {
        return jdbcTemplate.update(SQLConstants.SQL_INSERT_STUDENT, new Object[]{
                student.getId(),
                student.getName(),
                student.getSurname(),
                student.getAddress()
                } )>0?true:false;
    }
}
