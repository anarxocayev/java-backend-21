package az.atlacademy.introjdbc.repository;

import az.atlacademy.introjdbc.model.Student;

import java.util.List;

public interface StudentDao {
    Student getStudentById(Long id);

    List<Student> getAllStudents();

    boolean deletePerson(Student student);

    boolean updatePerson(Student student);

    boolean createPerson(Student student);
}
