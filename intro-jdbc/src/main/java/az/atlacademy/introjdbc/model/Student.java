package az.atlacademy.introjdbc.model;
//dto  - controller - data transfer object
//dao  - repository -  data access object

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    private Long id;
    private String name;
    private String surname;
    private String email;
    private String address;
    private String username;
    private String password;

}
