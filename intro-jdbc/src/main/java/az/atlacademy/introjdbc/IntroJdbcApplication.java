package az.atlacademy.introjdbc;

import az.atlacademy.introjdbc.model.Student;
import az.atlacademy.introjdbc.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class IntroJdbcApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(IntroJdbcApplication.class, args);
    }

    private final StudentRepository studentRepository;

    @Override
    public void run(String... args) throws Exception {
        // System.out.println(studentRepository.getStudentById(1l));
        //System.out.println(studentRepository.getAllStudents());
        Student s = new Student();
        s.setName("AAAA");
        s.setSurname("BBB");
        s.setAddress("CCCC");
        s.setId(23L);
        System.out.println(studentRepository.createPerson(s));

    }
}
