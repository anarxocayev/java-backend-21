package az.atlacademy.demo.model.entity;


public enum  RoleName {

    ROLE_USER,
    ROLE_ADMIN,
    ROLE_OPERATOR
}
