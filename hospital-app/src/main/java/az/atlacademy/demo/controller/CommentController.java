package az.atlacademy.demo.controller;


import az.atlacademy.demo.controller.request.CommentPayload;
import az.atlacademy.demo.model.entity.Comment;
import az.atlacademy.demo.security.CurrentUser;
import az.atlacademy.demo.security.UserPrincipal;
import az.atlacademy.demo.servise.CommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.UsesSunHttpServer;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/comments")
@RestController
@RequiredArgsConstructor
@Slf4j
@Api("Comment CRUD operations ")
@CrossOrigin
public class CommentController {
    private final CommentService commentService;

    @ApiOperation(value = "Get all comments")
    @GetMapping
    public ResponseEntity<List<Comment>> getAll() {
        return ResponseEntity.ok(commentService.findAll());
    }

    @ApiOperation("Create comment ")
    @PostMapping
    @PreAuthorize("hasRole('USER') ")
    public  ResponseEntity<CommentPayload> create(@CurrentUser UserPrincipal userPrincipal,
            @RequestBody CommentPayload commentPayload
    ){
        log.info("comment  {} ",commentPayload);

        if(commentService.create(commentPayload,userPrincipal)){
            return   ResponseEntity.status(HttpStatus.CREATED).body(commentPayload) ;
        }else{
            return   ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null) ;
        }

    }

    @ApiOperation("Create comment ")
    @PutMapping("/publish/{commentId}/{status}")
    @PreAuthorize("hasRole('ADMIN') ")
    public  ResponseEntity<Boolean> publishStatus(
            @PathVariable("commentId")  Long commentId,
            @PathVariable("status")  Integer status
    ){
        log.info("comment  {} ",commentId);
        log.info("status  {} ",status);

        if(commentService.publish(commentId,status)){
            return   ResponseEntity.status(HttpStatus.CREATED).body(Boolean.TRUE) ;
        }else{
            return   ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Boolean.FALSE) ;
        }

    }

}
