package az.atlacademy.demo.controller;


import az.atlacademy.demo.controller.response.BaseResponse;
import az.atlacademy.demo.controller.response.UserIdentityAvailability;
import az.atlacademy.demo.controller.response.UserProfile;
import az.atlacademy.demo.controller.response.UserSummary;
import az.atlacademy.demo.model.entity.User;
import az.atlacademy.demo.model.enums.BRStatus;
import az.atlacademy.demo.repository.UserRepository;
import az.atlacademy.demo.security.CurrentUser;
import az.atlacademy.demo.security.ResourceNotFoundException;
import az.atlacademy.demo.security.UserPrincipal;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class UserController {


    private final UserRepository userRepository;

    @GetMapping("/user/me")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER') or hasRole('OPERATOR')")
    public UserSummary getCurrentUser(@CurrentUser UserPrincipal currentUser) {
        UserSummary userSummary = new UserSummary(currentUser.getId(), currentUser.getUsername(), currentUser.getName());
        return userSummary;
    }

    @GetMapping("/user/checkUsernameAvailability")
    public UserIdentityAvailability checkUsernameAvailability(@RequestParam(value = "username") String username) {
        Boolean isAvailable = !userRepository.existsByUsername(username);
        return new UserIdentityAvailability(isAvailable);
    }

    @GetMapping("/user/checkEmailAvailability")
    public UserIdentityAvailability checkEmailAvailability(@RequestParam(value = "email") String email) {
        Boolean isAvailable = !userRepository.existsByEmail(email);
        return new UserIdentityAvailability(   isAvailable);
    }

    @GetMapping("/user/{username}")
    public UserProfile getUserProfile(@PathVariable(value = "username") String username) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new ResourceNotFoundException("User", "username", username));
        UserProfile userProfile = new UserProfile(user.getId(), user.getUsername(), user.getName(), user.getCreatedAt());
        return userProfile;
    }

    @GetMapping("/users")
    @PreAuthorize("hasRole('ADMIN') ")
    @ApiOperation(value = "", authorizations = { @Authorization(value="jwtToken") })
    public BaseResponse getUserList() {
        BaseResponse baseResponse=new BaseResponse();
        baseResponse.setMessage(BRStatus.SUCCESS.toString());
        baseResponse.setData( userRepository.findAll());
        return baseResponse;
    }






}