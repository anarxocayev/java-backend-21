package az.atlacademy.demo.servise;

import az.atlacademy.demo.controller.response.BaseResponse;
import az.atlacademy.demo.exception.HospitalNotFoundException;
import az.atlacademy.demo.model.entity.Comment;
import az.atlacademy.demo.model.entity.Hospital;
import az.atlacademy.demo.repository.CommentRepository;
import az.atlacademy.demo.repository.HospitalRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class HospitalService {
    private final HospitalRepository hospitalRepository;
    private final CommentRepository commentRepository;

    private final Logger LOGGER = Logger.getLogger(HospitalService.class.getName());

    public BaseResponse<List<Hospital>> findAll() {
        LOGGER.info("Started.. find all");
        BaseResponse<List<Hospital>> baseResponse=new BaseResponse<>();

        List<Hospital> hospitals = hospitalRepository.findAll();
        hospitals.forEach(h -> {
            List<Comment> comments = commentRepository.findAllByHospitalId(h.getId());
            h.setCommentCount(comments.size());
            h.setCommentList(comments);
        });
        LOGGER.info(String.format("all hopitals  - %s", hospitals));
        baseResponse.setData(hospitals);

        return baseResponse;
    }

    public BaseResponse<Hospital> findById(Long id) {
        LOGGER.info("Started.. find by id = " + id);
        BaseResponse<Hospital> baseResponse=new BaseResponse<>();
        try {
          Hospital  hospital = hospitalRepository.findById(id).get();

            hospital.setCommentList(commentRepository.findAllByHospitalId(hospital.getId())
                    .stream().filter(c->c.getIsPublished()==1).collect(Collectors.toList()));
            LOGGER.info(String.format(" hopital info  - %s", hospital));
            baseResponse.setData(hospital);
            baseResponse.setMessage("Success");

        } catch (Exception e) {
            LOGGER.info(e.getMessage());
           // e.printStackTrace();
            throw new HospitalNotFoundException(id);
        }


        return baseResponse;
    }


    public boolean create(Hospital hospital) {
        LOGGER.info("Started.. create hospital = " + hospital);
        boolean status = false;
        try {
            status = hospitalRepository.save(hospital).getId()!=null;


            LOGGER.info(String.format(" hopital created  - %s", hospital));
        } catch (Exception e) {
            throw new RuntimeException();
          //  LOGGER.log(Level.WARNING, "Error {}", e.getMessage());
        }

        return status;
    }
}
