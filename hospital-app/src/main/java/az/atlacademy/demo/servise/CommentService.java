package az.atlacademy.demo.servise;

import az.atlacademy.demo.controller.request.CommentPayload;
import az.atlacademy.demo.model.entity.Comment;
import az.atlacademy.demo.model.entity.Hospital;
import az.atlacademy.demo.repository.CommentRepository;
import az.atlacademy.demo.repository.HospitalRepository;
import az.atlacademy.demo.security.UserPrincipal;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
public class CommentService {

    private final CommentRepository commentRepository;
    private final HospitalRepository hospitalRepository;

    public List<Comment> findAll() {
        return commentRepository.findAll();
    }


    public boolean create(CommentPayload commentPayload, UserPrincipal userPrincipal) {
        log.info("Create comment  {}", commentPayload);
        Comment comment=new Comment();
        comment.setAuthor(userPrincipal.getUsername());
        comment.setEmail(userPrincipal.getEmail());
        comment.setComment(commentPayload.getComment());
        comment.setIsPublished(0);

        Hospital hospital=hospitalRepository.findById(commentPayload.getHospitalId()).get();
        comment.setHospital(hospital);
        boolean res = commentRepository.save(comment)!=null;
        log.info("result {}", res);
        return res;

    }

    public boolean publish(Long commentId, Integer status) {
        boolean res=true;
        try {
            Comment comment = commentRepository.findById(commentId).get();

            comment.setIsPublished(status);
            commentRepository.save(comment);

        }catch (Exception e){
            e.printStackTrace();;
            res=false;
        }
        return res;
    }

//    public boolean publish(Long commentId, Integer publish) {
//        log.info("Publish comment  {}", commentId);
//        //  Hospital hospital=hospitalRepository.getHospitalById(comment.getHospitalId());
//        boolean res = commentRepository.publish(commentId,publish);
//        log.info("result {}", res);
//        return res;
//
//    }



}
