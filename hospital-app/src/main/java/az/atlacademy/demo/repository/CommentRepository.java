package az.atlacademy.demo.repository;

import az.atlacademy.demo.model.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository

public interface CommentRepository extends JpaRepository<Comment, Long> {
    List<Comment> findAllByHospitalId(Long hospitalId);
}
