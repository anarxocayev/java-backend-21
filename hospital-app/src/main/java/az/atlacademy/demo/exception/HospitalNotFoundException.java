package az.atlacademy.demo.exception;

public class HospitalNotFoundException extends RuntimeException {
    public HospitalNotFoundException(Long id){
       super(String.format("Hospital not found with id = %s",id));
    }


}
