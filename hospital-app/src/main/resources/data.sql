INSERT INTO public.hospital (id, address, comment_count, created_at, email, image, name, phone, updated_at) VALUES (1, 'Bakı şəh. Əcəmi Naxçıvani 53', null, null, 'office@mederahospital.az', null, 'Medera Hospital', '992', null);

INSERT INTO public.comments (id, author, comment, email, is_published, hospital_id) VALUES (1, 'Anar Xocayev', 'Test comment 1', 'anarxocayev@gmail.com', 1, 1);
INSERT INTO public.comments (id, author, comment, email, is_published, hospital_id) VALUES (2, 'Amil Babayev', 'Test comment 2', 'amilbabayeva@gmail.com', 1, 1);