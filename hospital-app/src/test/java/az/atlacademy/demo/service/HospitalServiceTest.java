package az.atlacademy.demo.service;

import az.atlacademy.demo.controller.response.BaseResponse;
import az.atlacademy.demo.exception.HospitalNotFoundException;
import az.atlacademy.demo.model.entity.Comment;
import az.atlacademy.demo.model.entity.Hospital;
import az.atlacademy.demo.repository.CommentRepository;
import az.atlacademy.demo.repository.HospitalRepository;
import az.atlacademy.demo.servise.HospitalService;
import javassist.NotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class HospitalServiceTest {


    @Mock
    HospitalRepository hospitalRepository;
    @Mock
    CommentRepository commentRepository;

    @InjectMocks
    HospitalService hospitalService;


    @Test
    void testObjectNonNull() {
        assertNotNull(hospitalService);
        assertNotNull(commentRepository);
        assertNotNull(hospitalRepository);
    }

    @Test
    void testFindAllSuccess() {
        when(hospitalRepository.findAll()).thenReturn(getList());

        BaseResponse<List<Hospital>> listBaseResponse = hospitalService.findAll();

        assertEquals(listBaseResponse.getData().size(), 2);

    }

    @Test
    void testFindByIdSuccess() {
        when(hospitalRepository.findById(1L)).thenReturn(getHospital());
        BaseResponse<Hospital> findById = hospitalService.findById(1L);
        assertEquals(findById.getData().getAddress(), "Address");
        assertEquals(findById.getData().getName(), "ABC");
    }

    @Test
    void testFindByIdNotFoundException() {
        when(hospitalRepository.findById(1L)).thenThrow(new HospitalNotFoundException(1L));
        assertThrows(HospitalNotFoundException.class, () -> hospitalService.findById(1L));
    }

    @Test
    void testCreateHospitalSuccess(){
        Hospital hospital=getHospital().get();
        hospital.setId(1L);

        when(hospitalRepository.save(any())).thenReturn(hospital);
        assertTrue(hospitalService.create(hospital));

    }
    @Test
    void testCreateHospitalException(){
        Hospital hospital=getHospital().get();

        when(hospitalRepository.save(hospital)).thenThrow(new RuntimeException());

        assertThrows(RuntimeException.class, () -> hospitalService.create(hospital));

    }


    List<Hospital> getList() {
        List<Hospital> hospitals = new ArrayList<>();
        Hospital hospital1 = new Hospital();
        Hospital hospital2 = new Hospital();

        Comment comment = new Comment();
        comment.setComment("Comment 1");
        comment.setAuthor("Anar Xocayev");


        hospitals.add(hospital1);
        hospitals.add(hospital2);

        return hospitals;
    }

    Optional<Hospital> getHospital() {
        Hospital hospital1 = new Hospital();
        //hospital1.setId(1L);
        hospital1.setName("ABC");
        hospital1.setAddress("Address");
        Comment comment = new Comment();
        comment.setComment("Comment 1");
        comment.setAuthor("Anar Xocayev");
        hospital1.getCommentList().add(comment);
        return Optional.of(hospital1);
    }


}
