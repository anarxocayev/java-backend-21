package az.atlacademy.mvcapp.mvcwiththymleaf.task;


import net.javacrumbs.shedlock.core.SchedulerLock;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class Task {


    //@Scheduled(fixedRate = 1000)\
   @Scheduled(cron = "0 * * * * 1-5")
   @SchedulerLock(name = "send_email", lockAtLeastForString = "PT1M", lockAtMostForString = "PT2M")
   public void printDate() {

        System.out.println(LocalDateTime.now());
    }


}
