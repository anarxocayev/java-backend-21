package az.atlacademy.mvcapp.mvcwiththymleaf.controller;


import az.atlacademy.mvcapp.mvcwiththymleaf.entity.Student;
import az.atlacademy.mvcapp.mvcwiththymleaf.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

@Controller
@RequestMapping("/file")
@Slf4j
@RequiredArgsConstructor
public class FileController {

    private final StudentRepository studentRepository;

    @PostMapping("/upload")
    public String upload(@RequestPart String id, @RequestPart MultipartFile file) {
        // Get the name of the file
        System.out.println(id);
        Student student = studentRepository.getById(Long.valueOf(id));

        String filename = file.getOriginalFilename();//test.pdf
        // Get the file extension
        log.info("Original file name {}", filename);

        String suffix = filename.substring(filename.lastIndexOf("."));
        // upload files on upload file folder under the D drive
        log.info("Suffix {}", suffix);

        String path = "/Users/anarxocayev/Develop/";
        // prevent duplicate file names random file name
        String newFileName = UUID.randomUUID() + suffix;
        ;
        filename = path + newFileName;
        student.setImageName(newFileName);
        studentRepository.save(student);
        log.info("new file name {}", filename);
        File f = new File(filename);
        // If you do not upload a folder under the D drive to create a
        if (!f.getParentFile().exists()) {
            f.getParentFile().mkdirs();
        }
        try {
            // File type into the MultipartFile
            file.transferTo(f);
            return "redirect:/students/" + id;
        } catch (IOException e) {
            e.printStackTrace();
            return "error";
        }finally {

        }

    }

    @GetMapping(value = "/download/{fileName:.+}")
    public void download(@PathVariable("fileName") String fileName, HttpServletResponse response) {
        try {
            // file address, the real environment is stored in the database
            File file = new File(fileName);
            // Create input stream, the incoming file objects
            FileInputStream fis = new FileInputStream("/Users/anarxocayev/Develop/" + file);
            // set the relevant format
            response.setContentType("application/jpeg");
            // set the file name and download header
            response.addHeader("Content-disposition", "attachment;filename=" + file.getName());
            OutputStream os = response.getOutputStream();
            // normal operation
            byte[] buf = new byte[1024];
            int len = 0;
            while ((len = fis.read(buf)) != -1) {
                os.write(buf, 0, len);
            }
            os.close();
            fis.close();
           // return "success"; // In order to facilitate the testing I wrote a two html success.html there is a error.html is used to indicate the success or failure
        } catch (IOException e) {
            e.printStackTrace();
          //  return "error";
        }
    }


    @GetMapping(value = "/images/{fileName:.+}")
    public void images(@PathVariable("fileName") String fileName, HttpServletResponse response) {
        try {
            // file address, the real environment is stored in the database
            File file = new File(fileName);
            // Create input stream, the incoming file objects
            FileInputStream fis = new FileInputStream("/Users/anar/Develop/" + file);
            // set the relevant format
            response.setContentType("application/jpeg");
            // set the file name and download header
            response.addHeader("Content-disposition", "attachment;filename=" + file.getName());
            OutputStream os = response.getOutputStream();
            // normal operation
            byte[] buf = new byte[1024];
            int len = 0;
            while ((len = fis.read(buf)) != -1) {
                os.write(buf, 0, len);
            }
            os.close();
            fis.close();
            //return "success"; // In order to facilitate the testing I wrote a two html success.html there is a error.html is used to indicate the success or failure
        } catch (IOException e) {
            e.printStackTrace();
            // return "error";
        }
    }
}
