package az.atlacademy.mvcapp.mvcwiththymleaf.service;

import az.atlacademy.mvcapp.mvcwiththymleaf.entity.Student;
import az.atlacademy.mvcapp.mvcwiththymleaf.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@RequiredArgsConstructor
public class StudentService {
    private final StudentRepository studentRepository;

    public List<Student> getList() {
        return studentRepository.findAll();
    }

    public void add(Student s) {
        studentRepository.save(s);
    }

    public Student getById(Long id){
        return  studentRepository.getById(id);
    }

    public void delete(Long studentId) {
        Student student=getById(studentId);
        studentRepository.delete(student);
    }
}
