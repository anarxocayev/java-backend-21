package az.atlacademy.mvcapp.mvcwiththymleaf.validator;

import az.atlacademy.mvcapp.mvcwiththymleaf.entity.Student;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
@Component
public class StudentValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return false;
    }

    @Override
    public void validate(Object target, Errors errors) {
        Student s = (Student) target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "surname", "NotEmpty");
        System.out.println("validate "+s);
        if (s.getName() == null || s.getName().length() < 2 ||s.getName().isEmpty() || s.getName().isBlank()) {
            errors.rejectValue("name", "student.name.length.error");
        }

    }
}
