package az.atlacademy.mvcapp.mvcwiththymleaf;

import az.atlacademy.mvcapp.mvcwiththymleaf.entity.User;
import az.atlacademy.mvcapp.mvcwiththymleaf.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class MvcWithThymleafApplication implements CommandLineRunner {
final  UserService userService;
    public static void main(String[] args) {
        SpringApplication.run(MvcWithThymleafApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        User user=new User();
        user.setPassword("test1234");
        user.setFullName("Anar Xocayev");
        user.setUsername("axocayev");
        user.setEmail("anarxcoayev@gmail.om");

      //  userService.save(user);

    }
}
