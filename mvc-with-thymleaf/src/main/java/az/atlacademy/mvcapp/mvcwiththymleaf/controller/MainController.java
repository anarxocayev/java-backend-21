package az.atlacademy.mvcapp.mvcwiththymleaf.controller;


import az.atlacademy.mvcapp.mvcwiththymleaf.entity.Student;
import az.atlacademy.mvcapp.mvcwiththymleaf.service.MainService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class MainController {
    private String title = "My First MVC app";
    // private  Student st = new Student(1L, "Anar Xocayev", "Xirdalan,Haydar Aliyev pr", "anarxocayev@gmail.com");

    private final MainService mainService;

    @GetMapping("/switch")
    public String getHomePage() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String currentUserName = authentication.getName();
            System.out.println(authentication.getAuthorities());
            System.out.println("currentUserName "+currentUserName);
            List<String> auth=new ArrayList<>();
            for ( GrantedAuthority grantedAuthority :authentication.getAuthorities()){
                auth.add(grantedAuthority.getAuthority());
            }

            if(auth.contains("ROLE_USER")){
                System.out.println("redirect:students");
                return "redirect:/users/dashboard";
            }else{
                System.out.println("redirect:/");
                return "redirect:/admin/dashboard";
            }
        }

          return "redirect:/";
    }


}
