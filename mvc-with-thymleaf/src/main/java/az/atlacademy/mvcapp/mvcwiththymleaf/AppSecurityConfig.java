package az.atlacademy.mvcapp.mvcwiththymleaf;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class AppSecurityConfig extends WebSecurityConfigurerAdapter {


    private  final UserDetailsService userDetailsService;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/students", "/images/*","/testurl").permitAll()
               // .antMatchers("/admin/**").hasAuthority("ADMIN")
               // .antMatchers("/user/**").hasAuthority("USER")
              //  .anyRequest().authenticated()
                .and()
                .formLogin().loginPage("/admin/login").defaultSuccessUrl("/switch").permitAll()
                .and()
                .logout().invalidateHttpSession(true).logoutSuccessUrl("/admin/login")
                .permitAll();
    }

    @Bean
    public AuthenticationManager customAuthenticationManager() throws Exception {
        return authenticationManager();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
       /* auth.userDetailsService(userDetailsService)
                .passwordEncoder(bCryptPasswordEncoder);*/
        String password="test1234";
        String encPassword=this.bCryptPasswordEncoder.encode(password);

        auth.inMemoryAuthentication().withUser("anar").password(encPassword).roles("USER");
    }

}
