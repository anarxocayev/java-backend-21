package az.atlacademy.mvcapp.mvcwiththymleaf.controller;

import az.atlacademy.mvcapp.mvcwiththymleaf.entity.Student;
import az.atlacademy.mvcapp.mvcwiththymleaf.service.MainService;
import az.atlacademy.mvcapp.mvcwiththymleaf.service.StudentService;
import az.atlacademy.mvcapp.mvcwiththymleaf.validator.StudentValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;
import java.time.LocalDateTime;
@Controller
@RequiredArgsConstructor
@RequestMapping("/students")
public class StudentController {
    private String title = "My First MVC app";
    // private  Student st = new Student(1L, "Anar Xocayev", "Xirdalan,Haydar Aliyev pr", "anarxocayev@gmail.com");

    private final StudentService studentService;
private final StudentValidator studentValidator;

    @GetMapping("")
    public String getLIst(Model modelAndView,HttpServletRequest request,@RequestHeader("Authorization") String authToken) {
        System.out.println("Token : "+request.getHeader("Authorization"));
        System.out.println("authToken : "+authToken);
        modelAndView.addAttribute("students", studentService.getList());
        return "student-list";
    }

    @GetMapping("/{id}")
    public String getStudent(@PathVariable("id") Long id, Model modelAndView) {
        modelAndView.addAttribute("student", studentService.getById(id));
        return "student-view";
    }

    @PostMapping("")
    public String createStudent(@ModelAttribute Student student, BindingResult bindingResult) {

        System.out.println(student);
        studentValidator.validate(student,bindingResult);

        if (bindingResult.hasErrors()) {
            return "form";
        }
        studentService.add(student);
        return "redirect:/students";
    }

    @GetMapping("/student-add")
    public String addFormStudent(Model model) {
        model.addAttribute("student", new Student());
        return "form";
    }

    @GetMapping("/student-update/{id}")
    public String updateFormStudent(@PathVariable("id") Long id, Model model) {
        model.addAttribute("student", studentService.getById(id));
        return "form";
    }

    @GetMapping("/students-delete/")
    public String deleteStudent(@PathParam("studentId") String studentId) {
        studentService.delete(Long.parseLong(studentId));
        return "redirect:/students";
    }

}
