package az.atlacademy.mvcapp.mvcwiththymleaf.repository;

import az.atlacademy.mvcapp.mvcwiththymleaf.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student,Long> {
}
