package az.shop.ordersms.service;
//https://www.vogella.com/tutorials/Mockito/article.html
import az.shop.ordersms.client.ItemClient;
import az.shop.ordersms.client.UserClient;
import az.shop.ordersms.dto.OrderDto;
import az.shop.ordersms.entity.Order;
import az.shop.ordersms.entity.OrderItem;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {

    @Mock
    OrderService orderService;


    @Test
    void testIsOrderServiceNotNull() {
        assertNotNull(orderService);
    }

    @Test
    void testOrderTotal(){
       // orderService.order=getOrder();

           // Double total=orderService.getTotal(getOrder().getOrderItems());
        when(orderService.getTotal(getOrder().getOrderItems())).thenReturn(272.0);

        assertEquals(getOrder().getTotal(),orderService.getTotal(getOrder().getOrderItems()));

    }


    private Order getOrder() {
        Order order = new Order();
        order.setId(1L);
        order.setTotal(272.00);
        order.setUserId(2L);

        List<OrderItem> orderItems = List.of(
                new OrderItem(1L, 1L, 2, 16.00, 32.00),
                new OrderItem(12L, 2L, 3, 16.00, 240.00)
        );

        order.setOrderItems(orderItems);
        return order;
    }

}
