package az.shop.ordersms.util;


import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DateUtilTest {
    DateUtil dateUtil = new DateUtil();

    @Test
    void testConvertStringToLocalDateSuccess() {
        LocalDate now = LocalDate.of(2022, 1, 25);
        LocalDate localDate = dateUtil.convertStringToLocalDate("25/01/2022");
        assertEquals(now, localDate);
    }

    @Test
    void testConvertStringToLocalDateFormatException() {

        DateTimeParseException dateTimeParseException =
                assertThrows(DateTimeParseException.class, () ->
                        dateUtil.convertStringToLocalDate("25.01.2022"));

        String expectedMessage = "could not be parsed at index";
        System.out.println(expectedMessage);
        String actualMessage = dateTimeParseException.getMessage();
        System.out.println(actualMessage);
        assertTrue(actualMessage.contains(expectedMessage));

    }

    @Test
    void testConvertStringToLocalDateFormatIsNotNull() {

     assertNotNull(dateUtil);

    }
}
