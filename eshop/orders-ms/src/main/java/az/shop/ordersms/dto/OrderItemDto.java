package az.shop.ordersms.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderItemDto {
    private Long id;
    private Long itemId;
    private Integer count;
    private Double unitPrice;
    private Double totalPrice;
    private String name;
    private String firm;
}
