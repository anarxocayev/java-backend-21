package az.shop.ordersms.dto;

import az.shop.ordersms.client.model.User;
import az.shop.ordersms.entity.OrderItem;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDto {

    private Long id;
    private User user;
    private Double total;
    private String orderStatus;
    private List<OrderItemDto> items;

}
