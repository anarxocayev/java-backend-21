package az.shop.ordersms.conf;

import az.shop.ordersms.client.ItemClient;
import az.shop.ordersms.client.UserClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(clients = {UserClient.class, ItemClient.class})
public class AppConfig {
}
