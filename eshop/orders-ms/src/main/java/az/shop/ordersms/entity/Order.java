package az.shop.ordersms.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor

public class Order {
    private  Long id;
    private Long userId;
    private Double total;

   private List<OrderItem > orderItems;
}
