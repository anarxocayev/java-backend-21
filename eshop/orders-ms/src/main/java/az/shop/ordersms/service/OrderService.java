package az.shop.ordersms.service;

import az.shop.ordersms.client.ItemClient;
import az.shop.ordersms.client.UserClient;
import az.shop.ordersms.client.model.Item;
import az.shop.ordersms.dto.OrderDto;
import az.shop.ordersms.dto.OrderItemDto;
import az.shop.ordersms.entity.Order;
import az.shop.ordersms.entity.OrderItem;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderService {
    private final UserClient userClient;
    private final ItemClient itemClient;
   public Order order;

    {
        order = new Order();
        order.setId(1L);
        order.setTotal(272.00);
        order.setUserId(2L);

        List<OrderItem> orderItems = List.of(
                new OrderItem(1L, 1L, 2, 16.00, 32.00),
                new OrderItem(12L, 2L, 3, 16.00, 240.00)
        );

        order.setOrderItems(orderItems);

    }

    public OrderDto getOrder(Long orderId) {


        return mapToDto(order);
    }

    private OrderDto mapToDto(Order order) {
        OrderDto orderDto = new OrderDto();
        orderDto.setId(order.getId());
        orderDto.setTotal(getTotal(order.getOrderItems()));
        orderDto.setOrderStatus("COMPLETED");

        orderDto.setUser(userClient.getUser(order.getUserId()));

        orderDto.setItems(order.getOrderItems().stream().map(this::mapToDto).collect(Collectors.toList()));
        return orderDto;
    }

    public Double getTotal(List<OrderItem> items){
        return items.stream().map(i->i.getTotalPrice()).collect(Collectors.summingDouble(Double::doubleValue));
    }

    private OrderItemDto mapToDto(OrderItem orderItem) {

        OrderItemDto orderItemDto = new OrderItemDto();
        Item item = itemClient.getItem(orderItem.getItemId());

        orderItemDto.setId(orderItem.getId());
        orderItemDto.setItemId(orderItem.getItemId());

        orderItemDto.setName(item.getName());
        orderItemDto.setFirm(item.getFirm());

        orderItemDto.setCount(orderItem.getCount());
        orderItemDto.setTotalPrice(orderItem.getTotalPrice());
        orderItemDto.setUnitPrice(orderItem.getUnitPrice());

        return orderItemDto;
    }
}
