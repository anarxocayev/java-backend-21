package az.shop.ordersms.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderItem {
    private Long id;
    private Long itemId;
    private Integer count;
    private Double unitPrice;
    private Double totalPrice;
}
