package az.shop.ordersms.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class DateUtil {
    public  LocalDate convertStringToLocalDate(String date) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
            return LocalDate.parse(date, formatter);

    }

    public static void main(String[] args) {
       // convertStringToLocalDate("25/01/2022");
    }

}
