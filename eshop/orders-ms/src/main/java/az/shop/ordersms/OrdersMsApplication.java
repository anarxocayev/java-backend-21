package az.shop.ordersms;

import az.shop.ordersms.client.ItemClient;
import az.shop.ordersms.client.UserClient;
import az.shop.ordersms.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
@RequiredArgsConstructor
public class OrdersMsApplication implements CommandLineRunner {
    private final UserClient userClient;
    private final ItemClient itemClient;
    private final OrderService orderService;

    public static void main(String[] args) {
        SpringApplication.run(OrdersMsApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println(userClient.getUser(2L));
        System.out.println(itemClient.getItem(2L));
        System.out.println(orderService.getOrder(1L));

    }
}
