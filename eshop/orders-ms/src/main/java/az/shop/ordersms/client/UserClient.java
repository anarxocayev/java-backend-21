package az.shop.ordersms.client;

import az.shop.ordersms.client.model.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "users",url = "localhost:8081/api/v1/users")
public interface UserClient {
    @GetMapping("/{userId}")
     User getUser(@PathVariable("userId") Long userId);
}
