package az.shop.ordersms.client;

import az.shop.ordersms.client.model.Item;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
@FeignClient(value = "items",url = "localhost:8082/api/v1/items")
public interface ItemClient {
    @GetMapping("/{itemId}")
    public Item getItem(@PathVariable("itemId") Long itemId);
}
