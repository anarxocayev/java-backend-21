package az.eshop.usersms.controller;

import az.eshop.usersms.dto.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/api/v1/users")
@RestController
public class UserController {
    List<User> users;
    {
     users= List.of(
             new User(1L,"Anar","Xocayev","Enver","+994556112116"),
             new User(2L,"Zaur","Mustafazade","Enver","+994554567893"),
             new User(3L,"Ayhan","Hesenli","Enver","+99455698765"),
             new User(4L,"Vuqar","Ibrahimov","Enver","+99455876565")
     );
    }

    @GetMapping
    public List<User> getUsers(){
        return users;
    }

    @GetMapping("/{userId}")
    public  User  getUser(@PathVariable("userId") Long userId){
        return users.stream().filter(i->i.getId().equals(userId)).findFirst().get();
    }
 }
