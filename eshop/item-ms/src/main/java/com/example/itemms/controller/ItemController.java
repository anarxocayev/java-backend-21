package com.example.itemms.controller;

import com.example.itemms.dto.Item;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/items")
public class ItemController {
    List<Item> items;
    {
     items=List.of(
                new Item(1L,"4GB RAM","Kingstone",16.0,100),
                new Item(2L,"1TB HDD","Datagate",80.0,10),
                new Item(3L,"Wifi","TpLine",40.0,34)
        );
    }
    @GetMapping
    List<Item>  getItems(){
        return items;
    }
    @GetMapping("/{itemId}")
    public  Item  getItem(@PathVariable("itemId") Long itemId){
        System.out.println(items);
        return items.stream().filter(i->i.getId().equals(itemId)).findFirst().get();
    }
}
