package az.atlacademy.springbootjpaintro.service;


import az.atlacademy.springbootjpaintro.entity.Student;
import az.atlacademy.springbootjpaintro.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;

    public List<Student> getAll() {
        return studentRepository.findAll(Sort.by(Sort.Direction.DESC, "address", "name"));
    }

    public List<Student> getAll(String surname) {
        return studentRepository.findAllBySurname(surname).get();
    }

    public Student create(Student student) {
        return studentRepository.save(student);
    }

    public Student update(Student student) {
        Student studentForUpdate = studentRepository.getById(student.getId());
        studentForUpdate.setAddress(student.getAddress());
        studentForUpdate.setName(student.getName());

        return studentRepository.save(studentForUpdate);
    }

    public Student findById(Long id) {
        return studentRepository.findById(id).get();
    }

    public void delete(Long id) {
        Student student = studentRepository.getById(id);
        studentRepository.delete(student);
    }

}
