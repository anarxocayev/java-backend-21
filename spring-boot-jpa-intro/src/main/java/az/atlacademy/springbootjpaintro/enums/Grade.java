package az.atlacademy.springbootjpaintro.enums;

public enum  Grade {
    BACHELOR,
    MASTER,
    PHD
}
