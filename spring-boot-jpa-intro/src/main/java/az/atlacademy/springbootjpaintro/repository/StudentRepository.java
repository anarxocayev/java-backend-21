package az.atlacademy.springbootjpaintro.repository;

import az.atlacademy.springbootjpaintro.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
  Optional<List<Student>> findAllBySurname(String surname);

  @Query(value = "select * from students where surname = :surname", nativeQuery = true)
  List<Student> findAllBySurname2(@Param("surname") String surname);

  @Query(value = "delete from students where id=:id", nativeQuery = true)
  int deleteByIdX(@Param("id") Long id);

  @Query(value = "select s from Student s where s.id=:id")
  Student getById(@Param("id") Long id);

}
