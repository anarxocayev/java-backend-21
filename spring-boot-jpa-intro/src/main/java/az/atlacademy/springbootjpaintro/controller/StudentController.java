package az.atlacademy.springbootjpaintro.controller;

import az.atlacademy.springbootjpaintro.entity.Student;
import az.atlacademy.springbootjpaintro.service.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/students")
public class StudentController {
    private final StudentService studentService;

    @GetMapping
    public ResponseEntity<List<Student>> findall(){
        return ResponseEntity.ok(studentService.getAll());
    }
    @GetMapping("/{surname}")
    public ResponseEntity<List<Student>> findall(@PathVariable("surname")  String surname){
        return ResponseEntity.ok(studentService.getAll(surname));
    }
}
