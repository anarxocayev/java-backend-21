package az.atlacademy.springbootjpaintro;

import az.atlacademy.springbootjpaintro.entity.Student;
import az.atlacademy.springbootjpaintro.enums.Grade;
import az.atlacademy.springbootjpaintro.service.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.TypeCache;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.Sort;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class SpringBootJpaIntroApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootJpaIntroApplication.class, args);
    }

    private final StudentService studentService;


    @Override
    public void run(String... args) throws Exception {
        Student student=new Student();
        student.setId(null);
        student.setName("Kamil");
        student.setSurname("Aliyev");
        student.setAddress("Gence");
        student.setGrade(Grade.BACHELOR);
        student.setEmail("kamilaliyev@gmail.com");

         studentService.create(student);

        log.info("student  = {}",studentService.findById(1L));
        studentService.getAll().forEach(System.out::println);
    }
}
