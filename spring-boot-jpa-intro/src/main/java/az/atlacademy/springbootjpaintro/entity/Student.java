package az.atlacademy.springbootjpaintro.entity;


import az.atlacademy.springbootjpaintro.enums.Grade;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="students")
public class Student {
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String surname;
    private String email;
    private String address;
    @Column(name = "tution_fee")
    private Double fee;
    @Enumerated(value = EnumType.STRING)
    private Grade grade;

}

