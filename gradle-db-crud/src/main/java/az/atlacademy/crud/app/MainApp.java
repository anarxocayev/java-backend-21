package az.atlacademy.crud.app;


import az.atlacademy.crud.app.entity.Department;
import az.atlacademy.crud.app.entity.Employee;
import az.atlacademy.crud.app.repository.DepartmentRepository;
import az.atlacademy.crud.app.repository.EmployeeRepository;

import java.util.List;
import java.util.Scanner;

public class MainApp {
    private static EmployeeRepository employeeRepository = new EmployeeRepository();
    private static DepartmentRepository departmentRepository = new DepartmentRepository();

    public static void main(String[] args) {
        /*Department d=departmentRepository.findById(1);
        System.out.println(d);
        d.getEmployeeList().forEach(System.out::println);

        Employee e=employeeRepository.findById(3);
        System.out.println(e);
        System.out.println(e.getDepartment());*/

        do {
            System.out.println("1.New\n2.All\n3.Find by id\n4.Delete\n5.Update\n0.Exit");
            int s = new Scanner(System.in).nextInt();
            if (s == 2) {
                print(employeeRepository.findAll());
            }
            if (s == 1) {
                Employee e = getNewEmployee();
                System.out.println(employeeRepository.save(e));
            }
            if (s == 3) {
                findByid();
            }
            if (s == 4) {
                delete();
            }

            if (s == 5) {
                update();
            }
            if (s == 0) {
                break;
            }

        } while (true);


    }

    private static void update() {
        Scanner in = new Scanner(System.in);
        int id = in.nextInt();
        Employee e = employeeRepository.findById(id);
        System.out.println(getPrint(e));
        // 1.Name 2.surname
        String name = new Scanner(System.in).next();
        e.setName(name);
        employeeRepository.update(e);
    }

    private static void delete() {

        System.out.println("Enter Id: ");
        int id = new Scanner(System.in).nextInt();
        Employee e = employeeRepository.findById(id);
        employeeRepository.delete(e);
    }

    private static void findByid() {
        System.out.println("Enter Id: ");
        int id = new Scanner(System.in).nextInt();
        Employee e = employeeRepository.findById(id);
        System.out.println(getPrint(e));
    }


    static Employee getNewEmployee() {
        Employee e = new Employee();
        Scanner in = new Scanner(System.in);
        System.out.print("id : ");
        e.setId(Integer.parseInt(in.next()));
        System.out.print("name : ");
        e.setName(in.next());
        System.out.print("surname : ");
        e.setSurname(in.next());
        System.out.print("email : ");
        e.setEmail(in.next());
        System.out.print("phone : ");
        e.setPhone(in.next());
        System.out.print("address : ");
        e.setAddress(in.next());
        System.out.print("department id : ");
        // e.setDeptId(Integer.parseInt(in.next()));
        return e;
    }


    static void print(List<Employee> employeeList) {
        employeeList
                .stream()
                .forEach(e -> System.out.print(getPrint(e)));

    }

    static String getPrint(Employee e) {
        return String.format("%4d |%10s |%10s |%25s |%20s |%30s |%5d |\n",
                e.getId(), e.getName(), e.getSurname(), e.getEmail(),
                e.getPhone(), e.getAddress(), e.getDepartment().getId());
    }
}
