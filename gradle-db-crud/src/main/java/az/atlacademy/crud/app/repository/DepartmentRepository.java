package az.atlacademy.crud.app.repository;


import az.atlacademy.crud.app.config.DBConnection;
import az.atlacademy.crud.app.entity.Department;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DepartmentRepository {

    private DBConnection dbConnection = new DBConnection();
    private EmployeeRepository employeeRepository = new EmployeeRepository();

    public Department findById(int id) {

        try (Connection conn = dbConnection.getConnection()) {
            String sqlQuery = "select  * from departments where id=?";
            PreparedStatement ps = conn.prepareStatement(sqlQuery);
            ps.setInt(1, id);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                Department dept = new Department(resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("phone"),
                        new ArrayList<>()
                );


                dept.setEmployeeList(employeeRepository.findByDepartment(dept));
                return dept;

            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;

    }
}
