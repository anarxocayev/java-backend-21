package az.atlacademy.dockerdemo.dockerdemo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping( "/name")
public class MainController {
    private    List<String> nameList =new ArrayList<>();
     {
         nameList.add("Anar");
         nameList.add("Ayhan");
         nameList.add("Zaur");
         nameList.add("Meqsed");
         nameList.add("Nicat");
         nameList.add("Arif");
         nameList.add("Vuqar");
     }

    @GetMapping
    public ResponseEntity<List<String>> getNameList() {
        return ResponseEntity.ok(nameList);
    }

    @GetMapping("/{name}")
    public ResponseEntity<Integer> getNameId(@Valid

                                                 @PathVariable("name")
                                                 @Pattern(regexp="\\D" )
                                                         String name) {
        return ResponseEntity.ok(nameList.indexOf(name));
    }
    @PostMapping("/users")
    ResponseEntity<String> addUser(@Valid @RequestBody User user) {
        // persisting the user
        return ResponseEntity.ok("User is valid");
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
