package az.atlacademy.dockerdemo.dockerdemo.controller;


import javax.validation.constraints.*;

public class User {



    @NotBlank(message = "Name is mandatory")
    private String name;

    @Email(message = "Email format is incorrect")
    @NotBlank(message = "Email is mandatory" )
    private String email;
    @NotNull(message = "address must not be null")
    private String address;

    @Pattern(regexp="^[0-9]{2}[A-Z0-9]{5}",message="Pin must be 7 char or contains A-Z or 0-9")
    private String pin;

    @Min(value=18, message="must be equal or greater than 18")
    @Max(value=45, message="must be equal or less than 45")
    @NotNull
    private Integer age;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }



    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}